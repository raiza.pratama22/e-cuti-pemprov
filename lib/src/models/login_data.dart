import 'package:quiver/core.dart';
import '../helper/constants.dart';
import 'package:http/http.dart';

class LoginData {
  final String name;
  final String password;
  var loginUri = Uri.parse('${Constants.BASE_URL}/auth/signin');
  LoginData({required this.name, required this.password});

  Future<Response?> login(String username, String password) async {
    var res = await post(
        loginUri,
        body: {
          "nrk": username,
          "password": password
        }
    );
    return res;
  }

  @override
  String toString() {
    return 'LoginData($name, $password)';
  }

  @override
  bool operator ==(Object other) {
    if (other is LoginData) {
      return name == other.name && password == other.password;
    }
    return false;
  }

  @override
  int get hashCode => hash2(name, password);
}
