
import 'dart:convert';

import 'package:http/http.dart';
import 'package:login_example/helper/constants.dart';
import 'package:flutter/foundation.dart';

class AuthServices {
var loginUrl = Uri.parse('${Constants.BASE_URL}/login.php');

Future login(String nik, String password) async {
  var map = new Map<String, dynamic>();
  map['nrk'] = nik;
  map['password'] = password;
  var res = await post(
      loginUrl,
      body: map
  );
  var jsonResponse = jsonDecode(res.body);
  print("=================================== JSON Response===============================");
  print(jsonResponse);
  debugPrint('Name: ${jsonResponse}');

  print("=================================== JSON Response===============================");
  return jsonResponse;
}

}