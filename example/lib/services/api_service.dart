import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:login_example/helper/constants.dart';
import 'package:login_example/services/apiinterceptor.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class ApiService {
  Client client = InterceptedClient.build(interceptors: [
    ApiInterceptor(),
  ]);

  Future<Response> postPengjuanCuiti( String? id_cuti, String tgl_awal,
      String tgl_akhir, String jml_cuti, String alasan, String alamat_sementara) async {
    final storage = const FlutterSecureStorage();
      var pengajuanCuti = Uri.parse('${Constants.BASE_URL}/pengajuan_cuti.php');
      var maps = new Map<String, dynamic>();
      maps['token'] = await storage.read(key: "token");
      maps['_case'] = 'pengajuan';
      maps['id_cuti'] = id_cuti;
      maps['tgl_awal'] = tgl_awal;
      maps['tgl_akhir'] = tgl_akhir;
      maps['jml_cuti'] = jml_cuti;
      maps['alasan'] = alasan;
      maps['alamat_sementara'] = alamat_sementara;

    final res = await client.post(pengajuanCuti, body: maps);

    var jsonResponse = jsonDecode(res.body);
    print('==================================== post pengajuan cuti ======================');
    print(jsonResponse);
    print(await storage.read(key: "token"));
    return res;
  }
  Future<Response> approvel(String idCuti ) async {
    final storage = const FlutterSecureStorage();
    String formattedDate =  DateFormat('yyyy-MM-dd').format(DateTime.now());
    print(formattedDate);
    var pengajuanCuti = Uri.parse('${Constants.BASE_URL}/pengajuan_cuti.php');
    var maps = new Map<String, dynamic>();
    maps['token'] = await storage.read(key: "token");
    maps['_case'] = 'approval';
    maps['idCuti'] = idCuti;
    maps['status_approval'] = '1';
    maps['tgl_approve'] = formattedDate;
    maps['note'] = 'Karyawan Boleh Cuti';
    // String formattedDate =
    // DateFormat('yyyy-MM-dd').format(pickedDate);
     print(maps);
    final res = await client.post(pengajuanCuti, body: maps);

    var jsonResponse = jsonDecode(res.body);
    print(jsonResponse);
    print(await storage.read(key: "token"));
    return res;
  }
  Future<Response> reject(String idCuti ) async {
    final storage = const FlutterSecureStorage();
    String formattedDate =  DateFormat('yyyy-MM-dd').format(DateTime.now());
    print(formattedDate);
    var pengajuanCuti = Uri.parse('${Constants.BASE_URL}/pengajuan_cuti.php');
    var maps = new Map<String, dynamic>();
    maps['token'] = await storage.read(key: "token");
    maps['_case'] = 'approval';
    maps['idCuti'] = idCuti;
    maps['status_approval'] = '2';
    maps['tgl_approve'] = formattedDate;
    maps['note'] = 'Karyawan Tidak Boleh Cuti';
    // String formattedDate =
    // DateFormat('yyyy-MM-dd').format(pickedDate);
    print(maps);
    final res = await client.post(pengajuanCuti, body: maps);

    var jsonResponse = jsonDecode(res.body);
    print(jsonResponse);
    print(await storage.read(key: "token"));
    return res;
  }

  Future<Response> getUserList() async {
    var userUrl = Uri.parse('${Constants.BASE_URL}/users');
    final res = await client.get(userUrl);
    return res;
  }

  Future<Response> getUserById(String id) async {
    var userUrl = Uri.parse('${Constants.BASE_URL}/users/$id');
    final res = await client.get(userUrl);
    return res;
  }

  Future<Response> addUser(int roleId, String email, String password,
      String fullname, String phone) async {
    var userUrl = Uri.parse('${Constants.BASE_URL}/users');
    final res = await client.post(userUrl, body: {
      "role_id": roleId.toString(),
      "email": email,
      "password": password,
      "fullname": fullname,
      "phone": phone
    });
    return res;
  }

  Future<Response> updateUser(
      int? id, int roleId, String email, String fullname, String phone) async {
    var userUrl = Uri.parse('${Constants.BASE_URL}/users/$id');
    final res = await client.put(userUrl, body: {
      "role_id": roleId.toString(),
      "email": email,
      "fullname": fullname,
      "phone": phone
    });
    return res;
  }

  Future<Response> deleteUser(String id) async {
    var userUrl = Uri.parse('${Constants.BASE_URL}/users/$id');
    final res = await client.delete(userUrl);
    return res;
  }

  Future<Response> getRoleList() async {
    var rolerUrl = Uri.parse('${Constants.BASE_URL}/roles');
    final res = await client.get(rolerUrl);
    return res;
  }

  Future<Response> getRoleById(String id) async {
    var rolerUrl = Uri.parse('${Constants.BASE_URL}/roles/$id');
    final res = await client.get(rolerUrl);
    return res;
  }

  Future<Response> addRole(String roleName, String roleDescription) async {
    var rolerUrl = Uri.parse('${Constants.BASE_URL}/roles');
    final res = await client.post(rolerUrl,
        body: {"role_name": roleName, "role_description": roleDescription});
    return res;
  }

  Future<Response> updateRole(
      int? id, String roleName, String roleDescription) async {
    var rolerUrl = Uri.parse('${Constants.BASE_URL}/roles/$id');
    final res = await client.put(rolerUrl,
        body: {"role_name": roleName, "role_description": roleDescription});
    return res;
  }

  Future<Response> deleteRole(String id) async {
    var rolerUrl = Uri.parse('${Constants.BASE_URL}/roles/$id');
    final res = await client.delete(rolerUrl);
    return res;
  }

  Future<Response> addPermissiontoRole(int? id, List<int?> permissions) async {
    var permissionUrl =
    Uri.parse('${Constants.BASE_URL}/roles/permissions/$id');
    final res =
    await client.post(permissionUrl, body: {"permissions": jsonEncode(permissions)});
    return res;
  }

  Future<Response> getPermissionList() async {
    var permissionUrl = Uri.parse('${Constants.BASE_URL}/permissions');
    final res = await client.get(permissionUrl);
    return res;
  }

  Future<Response> getPermissionById(String id) async {
    var permissionUrl = Uri.parse('${Constants.BASE_URL}/permissions/$id');
    final res = await client.get(permissionUrl);
    return res;
  }

  Future<Response> addPermission(
      String permName, String permDescription) async {
    var permissionUrl = Uri.parse('${Constants.BASE_URL}/permissions');
    final res = await client.post(permissionUrl,
        body: {"perm_name": permName, "perm_description": permDescription});
    return res;
  }

  Future<Response> updatePermission(
      int id, String permName, String permDescription) async {
    var permissionUrl = Uri.parse('${Constants.BASE_URL}/permissions/$id');
    final res = await client.put(permissionUrl,
        body: {"perm_name": permName, "perm_description": permDescription});
    return res;
  }

  Future<Response> deletePermission(String id) async {
    var permissionUrl = Uri.parse('${Constants.BASE_URL}/permissions/$id');
    final res = await client.delete(permissionUrl);
    return res;
  }

  Future<Response> getProductList() async {
    var productUrl = Uri.parse('${Constants.BASE_URL}/products');
    final res = await client.get(productUrl);
    return res;
  }

  Future<Response> getProductById(String id) async {
    var productUrl = Uri.parse('${Constants.BASE_URL}/products/$id');
    final res = await client.get(productUrl);
    return res;
  }

  Future<Response> addProduct(String prodName, String prodDescription,
      String prodImage, String prodPrice) async {
    var productUrl = Uri.parse('${Constants.BASE_URL}/products');
    final res = await client.post(productUrl, body: {
      "prod_name": prodName,
      "prod_description": prodDescription,
      "prod_image": prodImage,
      "prod_price": prodPrice
    });
    return res;
  }

  Future<Response> updateProduct(int? id, String prodName,
      String prodDescription, String prodImage, String prodPrice) async {
    var productUrl = Uri.parse('${Constants.BASE_URL}/products/$id');
    final res = await client.put(productUrl, body: {
      "prod_name": prodName,
      "prod_description": prodDescription,
      "prod_image": prodImage,
      "prod_price": prodPrice
    });
    return res;
  }

  Future<Response> deleteProduct(String id) async {
    var productUrl = Uri.parse('${Constants.BASE_URL}/products/$id');
    final res = await client.delete(productUrl);
    return res;
  }
}
