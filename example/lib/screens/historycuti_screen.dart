import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_login/theme.dart';
import 'package:flutter_login/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:login_example/screens/second_screen.dart';

import '../helper/constants.dart';

class HistoryCutiScreen extends StatefulWidget {
  @override
  _HistoryCutiScreenState createState() => _HistoryCutiScreenState();
}


class _HistoryCutiScreenState extends State<HistoryCutiScreen> {
  final _formGlobalKey = GlobalKey<FormState>();
  String _selectedText = "";
  List<String> _favoriteColor = [
    "Red",
    "Blue",
    "White",
    "Green",
    "Black",
    "Orange",
    "Yellow",
    "Violet",
    "Other"
  ];
  final storage = FlutterSecureStorage();
  String? levels;
  String? names;
  Future<String?>  _getLevel() async {
    var token = await storage.read(key: "level");
    // print(token);
    var nameUser = await storage.read(key: "nama");

    setState(() {
      levels = token;
      names = nameUser;
    });
    print(names);
    return token;
  }

  List _dataListCutix = [];


  Future<List?> getSisaCuti() async {

    final storage = const FlutterSecureStorage();
    final loginUrl = Uri.parse('${Constants.BASE_URL}/pengajuan_cuti.php?token=${await storage.read(key: "token")}&_case=history');
    final response = await get(
        loginUrl
    );


    // http.Response response = await http.get(Uri.parse(applicantsByDate));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print('================================ reponse history cuti ===============================');
      print(data);

      print('================================ reponse history cuti ===============================');
      // Map<dynamic, String>? map = data;
      setState(() {
        // _dataListCuti = jsonEncode(map!["result"]);
        _dataListCutix = data['result'];
      });
      //
      _dataListCutix = data['result'];

      // print(map);
      return _dataListCutix;
    }}



  late FocusNode _phoneFocusNode, _emailFocusNode, _nameFocusNode;

  // Initialize Focus Node Objects in initState
  @override
  void initState() {
    super.initState();
    getSisaCuti();
    _getLevel();
    _phoneFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _nameFocusNode = FocusNode();
  }

  // Focus nodes are long living objects, so dispose it
  @override
  void dispose() {
    _phoneFocusNode.dispose();
    _emailFocusNode.dispose();
    _nameFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Hero(
                  tag: 'check',
                  child: Image.asset(
                    'assets/images/ecorp.png',
                    filterQuality: FilterQuality.high,
                    height: 30,
                  ),
                ),
              ),
              HeroText(
                Constants.historyCuti,
                tag: Constants.titleTag,
                viewState: ViewState.shrunk,
                style: LoginThemeHelper.loginTextStyle,
              ),
              const SizedBox(width: 20),
            ],
          ),
        ),
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
      ),
      body:
    SingleChildScrollView(
    scrollDirection: Axis.vertical,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: const <DataColumn>[
            // DataColumn(
            //   label: Expanded(
            //     child: Text(
            //       'NRK',
            //       style: TextStyle(fontStyle: FontStyle.italic),
            //     ),
            //   ),
            // ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Nama Pegawai',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Alasan Cuti',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Tanggal Mulai',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Tanggal Selesai',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Lama',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),
            DataColumn(
              label: Expanded(
                child: Text(
                  'Status',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              ),
            ),

          ],


          rows: List.generate(
            _dataListCutix.length,
                (index) => _resultsAPI(
              index,
              _dataListCutix[index],
            ),
          ),
        ),
      ),
    ),
    );



  }
  DataRow _resultsAPI(index, data) {
    return DataRow(
      cells: <DataCell>[
        DataCell(
          Text(
            data['nama_pegawai'],
          ),
        ), //add name of your columns here
        DataCell(
          Text(
            data['alasan_cuti'],
          ),
        ),
        DataCell(
          Text(
            data['tanggal_awal_cuti'],
          ),
        ),
        DataCell(
          Text(
            data['tanggal_akhir_cuti'],
          ),
        ),
        DataCell(
          Text(
            data['jumlah_cuti'],
          ),
        ),
        DataCell(
          Text(
            data['status_cuti'],
          ),
        )
      ],
    );
  }
}



// class MyStatelessWidget extends StatelessWidget {
//    const MyStatelessWidget( );
//
//   // SisaCutiScreen _sisaCutiScreen;
//
//   @override
//   Widget build(BuildContext context) {
//     return DataTable(
//       columns: const <DataColumn>[
//         DataColumn(
//           label: Expanded(
//             child: Text(
//               'Name',
//               style: TextStyle(fontStyle: FontStyle.italic),
//             ),
//           ),
//         ),
//         DataColumn(
//           label: Expanded(
//             child: Text(
//               'Jenis',
//               style: TextStyle(fontStyle: FontStyle.italic),
//             ),
//           ),
//         ),
//         DataColumn(
//           label: Expanded(
//             child: Text(
//               'Sisa',
//               style: TextStyle(fontStyle: FontStyle.italic),
//             ),
//           ),
//         ),
//         DataColumn(
//           label: Expanded(
//             child: Text(
//               'Tahun',
//               style: TextStyle(fontStyle: FontStyle.italic),
//             ),
//           ),
//         ),
//
//       ],
//
//       rows:  <DataRow>[
//         DataRow(
//           cells: <DataCell>[
//
//
//             DataCell(ListView.builder(
//               itemCount: 2,
//
//               itemBuilder: (BuildContext context, int index) {
//                 return ListTile(
//                     leading: const Icon(Icons.list),
//                     trailing: const Text(
//                       "GFG",
//                       style: TextStyle(color: Colors.green, fontSize: 15),
//                     ),
//                     title: Text("List item $index"));
//               }),),
//             // DataCell(Text('Pindah Rumah')),
//             // DataCell(Text('10')),
//             // DataCell(Text('2022')),
//           ],
//         ),
//       ],
//     );
//   }
// }

