import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_example/screens/dashboard_screen.dart';

class SecondScreen extends StatelessWidget {
  static const routeName = '/second-screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Success Notification",
        ),
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back,
              color: Colors.white),
          onPressed: () => {
          Navigator.push(context,
          MaterialPageRoute(builder: (context) => DashboardScreen()))
          }
             ) ,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              height: 160,
              child: SvgPicture.asset(
                'assets/images/mail.svg',
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
              child: Text(
                "Thank you for your response",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 8.0, bottom: 16.0, left: 8.0, right: 8.0),
              child: Text(
                "Our team will contact you soon.",
                textAlign: TextAlign.center,
              ),
            ),
            // RaisedButton(
            //   child: Text(
            //     "OK",
            //     style: TextStyle(color: Colors.white),
            //   ),
            //   onPressed: () {
            //     Navigator.of(context).pop();
            //   },
            // )
          ],
        ),
      ),
    );
  }
}
