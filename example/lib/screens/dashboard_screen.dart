import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_login/theme.dart';
import 'package:flutter_login/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:login_example/helper/constants.dart';
import 'package:login_example/screens/approve_screen.dart';
import 'package:login_example/widgets/transition_route_observer.dart';
import 'package:login_example/widgets/fade_in.dart';
import 'package:login_example/widgets/round_button.dart';
import 'package:login_example/screens/first_screen.dart';
import 'package:login_example/screens/sisacuti_screen.dart';
import 'package:login_example/screens/historycuti_screen.dart';
import 'package:login_example/screens/reportcuti_screen.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
class DashboardScreen extends StatefulWidget {
  static const routeName = '/dashboard';

  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with SingleTickerProviderStateMixin, TransitionRouteAware {
  Duration get loginTime => Duration(milliseconds: timeDilation.ceil() * 2250);
  final storage = const FlutterSecureStorage();
  String? levels;
  String? names;
  Future<bool> _goToLogin(BuildContext context) {
    return Navigator.of(context)
        .pushReplacementNamed('/auth')
        // we dont want to pop the screen, just replace it completely
        .then((_) => false);
  }
  Future<String?>  _getLevel() async {
    var token = await storage.read(key: "level");
    // print(token);
    var nameUser = await storage.read(key: "nama");

    setState(() {
      levels = token;
      names = nameUser;
    });
    // print(names);
    return token;
  }


  final routeObserver = TransitionRouteObserver<PageRoute?>();
  static const headerAniInterval = Interval(.1, .3, curve: Curves.easeOut);
  late Animation<double> _headerScaleAnimation;
  AnimationController? _loadingController;

  @override
  void initState() {
    super.initState();
    _getLevel();
    print(names);
    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1250),
    );

    _headerScaleAnimation = Tween<double>(begin: .6, end: 1).animate(
      CurvedAnimation(
        parent: _loadingController!,
        curve: headerAniInterval,
      ),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(
      this,
      ModalRoute.of(context) as PageRoute<dynamic>?,
    );
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    _loadingController!.dispose();
    super.dispose();
  }

  @override
  void didPushAfterTransition() => _loadingController!.forward();

  AppBar _buildAppBar(ThemeData theme) {
    final menuBtn = IconButton(
      color: theme.colorScheme.secondary,
      icon: const Icon(FontAwesomeIcons.bars),
      onPressed: () {},
    );
    final signOutBtn = IconButton(
      icon: const Icon(FontAwesomeIcons.rightFromBracket),
      color: theme.colorScheme.secondary,
      onPressed: () => _goToLogin(context),
    );
    final title = Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Hero(
              tag: Constants.logoTag,
              child: Image.asset(
                'assets/images/ecorp.png',
                filterQuality: FilterQuality.high,
                height: 30,
              ),
            ),
          ),
          HeroText(
            Constants.appName,
            tag: Constants.titleTag,
            viewState: ViewState.shrunk,
            style: LoginThemeHelper.loginTextStyle,
          ),
          const SizedBox(width: 20),
        ],
      ),
    );

    return AppBar(
      leading: FadeIn(
        controller: _loadingController,
        offset: .3,
        curve: headerAniInterval,
        child: menuBtn,
      ),
      actions: <Widget>[
        FadeIn(
          controller: _loadingController,
          offset: .3,
          curve: headerAniInterval,
          fadeDirection: FadeDirection.endToStart,
          child: signOutBtn,
        ),
      ],
      title: title,
      backgroundColor: theme.primaryColor.withOpacity(.1),
      elevation: 0,
      // toolbarTextStyle: TextStle(),
      // textTheme: theme.accentTextTheme,
      // iconTheme: theme.accentIconTheme,
    );
  }



  Widget _buildButton({
    Widget? icon,
    String? label,
    required Function() onPressed,
    required Interval interval,
  }) {
    return RoundButton(
      icon: icon,
      label: label,
      loadingController: _loadingController,
      interval: Interval(
        interval.begin,
        interval.end,
        curve: const ElasticOutCurve(0.42),
      ),
      onPressed: onPressed,
    );
  }

  Widget _buildDashboardGrid()     {
    const step = 0.04;
    const aniInterval = 0.75;
    Future<String?>  check() async {
      var token = await _getLevel();
      // print(token);
      levels = token;
      return token;
    }
    check();
    // print(levels);
    MaterialPageRoute route;
    return  GridView.count(
      padding: const EdgeInsets.symmetric(
        horizontal: 32.0,
        vertical: 20,
      ),
      childAspectRatio: .9,
      // crossAxisSpacing: 5,
      crossAxisCount: 3,
      children: levels == '1' ?  [

    // _buildButton(
    //   icon: const Icon(FontAwesomeIcons.chartLine),
    //   label: 'Report Cuti',
    //   interval: const Interval(0, aniInterval),
    //   onPressed: ()  {Navigator.push(context,
    //       MaterialPageRoute(builder: (context) => ReportCutiScreen()));
    //   },
    //
    // ),

      _buildButton(
          icon: const Icon(FontAwesomeIcons.user),
          label: 'Form Cuti',
          interval: const Interval(0, aniInterval),
          onPressed: ()  {

            Navigator.push(context,
                MaterialPageRoute(builder: (context) => FirstScreen()));
          },
        ), _buildButton(
          icon: Container(
            // fix icon is not centered like others for some reasons
            padding: const EdgeInsets.only(left: 16.0),
            alignment: Alignment.centerLeft,
            child: const Icon(
              FontAwesomeIcons.moneyBill1,
              size: 20,
            ),
          ),
          label: 'Sisa Cuti',
          interval: const Interval(step, aniInterval + step),
          onPressed: ()  {Navigator.push(context,
    MaterialPageRoute(builder: (context) => SisaCutiScreen()));
    },
        ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.handHoldingDollar),
        //   label: 'Payment',
        //   interval: const Interval(step * 2, aniInterval + step * 2),
        // ),

        // _buildButton(
        //   icon: const Icon(Icons.vpn_key),
        //   label: 'Register',
        //   interval: const Interval(step, aniInterval + step),
        // ),
        _buildButton(
          icon: const Icon(FontAwesomeIcons.clockRotateLeft),
          label: 'History Cuti',
          interval: const Interval(step * 2, aniInterval + step * 2),
          onPressed: ()  {Navigator.push(context,
              MaterialPageRoute(builder: (context) => HistoryCutiScreen()));
          },
        ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.ellipsis),
        //   label: 'Other',
        //   interval: const Interval(0, aniInterval),
        // ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.magnifyingGlass, size: 20),
        //   label: 'Search',
        //   interval: const Interval(step, aniInterval + step),
        // ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.sliders, size: 20),
        //   label: 'Settings',
        //   interval: const Interval(step * 2, aniInterval + step * 2),
        // ),
      ]
          :

      levels == '4' ?

      [

      // _buildButton(
      //   icon: const Icon(FontAwesomeIcons.chartLine),
      //   label: 'Report Cuti',
      //   interval: const Interval(0, aniInterval),
      //   onPressed: ()  {Navigator.push(context,
      //       MaterialPageRoute(builder: (context) => ReportCutiScreen()));
      //   },
      //
      // ),
        _buildButton(
          icon: const Icon(FontAwesomeIcons.clockRotateLeft),
          label: 'History Cuti',
          interval: const Interval(step * 2, aniInterval + step * 2),
          onPressed: ()  {Navigator.push(context,
              MaterialPageRoute(builder: (context) => HistoryCutiScreen()));
          },
        ),

        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.handHoldingDollar),
        //   label: 'Payment',
        //   interval: const Interval(step * 2, aniInterval + step * 2),
        // ),

        // _buildButton(
        //   icon: const Icon(Icons.vpn_key),
        //   label: 'Register',
        //   interval: const Interval(step, aniInterval + step),
        // ),

        _buildButton(
          icon: const Icon(FontAwesomeIcons.envelopeCircleCheck),
          label: 'Approval Cuti',
          interval: const Interval(step * 2, aniInterval + step * 2),
          onPressed: ()  {Navigator.push(context,
              MaterialPageRoute(builder: (context) => ApproveScreen()));
          },
        ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.ellipsis),
        //   label: 'Other',
        //   interval: const Interval(0, aniInterval),
        // ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.magnifyingGlass, size: 20),
        //   label: 'Search',
        //   interval: const Interval(step, aniInterval + step),
        // ),
        // _buildButton(
        //   icon: const Icon(FontAwesomeIcons.sliders, size: 20),
        //   label: 'Settings',
        //   interval: const Interval(step * 2, aniInterval + step * 2),
        // ),
        ]

          :

        [      _buildButton(
        icon: const Icon(FontAwesomeIcons.user),
        label: 'Form Cuti',
        interval: const Interval(0, aniInterval),
        onPressed: ()  {

          Navigator.push(context,
              MaterialPageRoute(builder: (context) => FirstScreen()));
        },
      ), _buildButton(
        icon: Container(
          // fix icon is not centered like others for some reasons
          padding: const EdgeInsets.only(left: 16.0),
          alignment: Alignment.centerLeft,
          child: const Icon(
            FontAwesomeIcons.moneyBill1,
            size: 20,
          ),
        ),
        label: 'Sisa Cuti',
        interval: const Interval(step, aniInterval + step),
        onPressed: ()  {Navigator.push(context,
            MaterialPageRoute(builder: (context) => SisaCutiScreen()));
        },
      ),   _buildButton(
        icon: const Icon(FontAwesomeIcons.clockRotateLeft),
        label: 'History Cuti',
        interval: const Interval(step * 2, aniInterval + step * 2),
        onPressed: ()  {Navigator.push(context,
            MaterialPageRoute(builder: (context) => HistoryCutiScreen()));
        },
      ),
        _buildButton(
        icon: const Icon(FontAwesomeIcons.envelopeCircleCheck),
        label: 'Approval Cuti',
        interval: const Interval(step * 2, aniInterval + step * 2),
        onPressed: ()  {Navigator.push(context,
            MaterialPageRoute(builder: (context) => ApproveScreen()));
        },
      ),]
    );
  }

  Widget _buildDebugButtons() {
    const textStyle = TextStyle(fontSize: 12, color: Colors.white);

    return Positioned(
      bottom: 0,
      right: 0,
      child: Row(
        children: <Widget>[
          MaterialButton(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            color: Colors.red,
            onPressed: () => _loadingController!.value == 0
                ? _loadingController!.forward()
                : _loadingController!.reverse(),
            child: const Text('loading', style: textStyle),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return WillPopScope(
      onWillPop: () => _goToLogin(context),
      child: SafeArea(
        child: Scaffold(
          appBar: _buildAppBar(theme),
          body: Container(

            width: double.infinity,
            height: double.infinity,
            color: theme.primaryColor.withOpacity(.1),
            child: Stack(
              children: <Widget>[
                Column(

                  children: <Widget>[
                    const SizedBox(height: 40),
                    // Row(
                    //   children: const <Widget>[
                    //     Expanded(child: Divider()),
                    //     Padding(
                    //       padding: EdgeInsets.all(0.0),
                    //       child: Text("Welcome to e-cuti application, E-Cuti User" + names!),
                    //     ),
                    //     Expanded(child: Divider()),
                    //   ],),
                    Container(
                      alignment: Alignment.bottomCenter,
                      padding: const EdgeInsets.only(left: 25.0),

                      child: Row(
                        children: <Widget>[
                          Text("Selamat datang bapak/ibu " + names!,  style: const TextStyle(
                        height: 0,
                        fontSize: 14.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      child: ShaderMask(
                        // blendMode: BlendMode.srcOver,
                        shaderCallback: (Rect bounds) {
                          return LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: <Color>[
                              Colors.deepPurpleAccent.shade100,
                              Colors.deepPurple.shade100,
                              Colors.deepPurple.shade100,
                              Colors.deepPurple.shade100,
                              // Colors.red,
                              // Colors.yellow,
                            ],
                          ).createShader(bounds);
                        },
                        child: _buildDashboardGrid(),
                      ),
                    ),
                  ],
                ),
                if (!kReleaseMode) _buildDebugButtons(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
