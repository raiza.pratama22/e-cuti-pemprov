import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_login/theme.dart';
import 'package:flutter_login/widgets.dart';
import 'package:http/http.dart';
import 'package:login_example/screens/second_screen.dart';
import 'package:login_example/services/api_service.dart';
import 'package:intl/intl.dart';
// import 'package:http/http.dart' as http;
import '../helper/constants.dart';
import 'package:flutter/foundation.dart';
class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}


class _FirstScreenState extends State<FirstScreen> {
  final ApiService api = ApiService();
  final _formGlobalKey = GlobalKey<FormState>();
  TextEditingController dateInput = TextEditingController();
  TextEditingController lastDateInput = TextEditingController();
  final _NIPController = TextEditingController();
  final _JumlahCutiController = TextEditingController();
  final _KeteranganController = TextEditingController();
  final _AlamatController = TextEditingController();
  String? _valJenisCuti;
  final loginUrl = Uri.parse('${Constants.BASE_URL}/list_cuti.php');
  List<dynamic> _dataListCuti = [];
  void getProvince() async {
    final res = await get(
        loginUrl
    );
    var listData = jsonDecode(res.body);
    print('============================= check list data =======================');
    print(listData);
    Map<String, dynamic>? map = listData;
    setState(() {
      _dataListCuti = map!["data"];
    });
  }

  String _selectedText = "";

  late FocusNode _nipFocusNode, _jenisFocusNode, _nameFocusNode;

  // Initialize Focus Node Objects in initState
  @override
  void initState() {
    super.initState();
    getProvince();
    dateInput.text = "";
    lastDateInput.text = "";//set the initial value of
    _nipFocusNode = FocusNode();
    _jenisFocusNode = FocusNode();
    _nameFocusNode = FocusNode();
  }

  // Focus nodes are long living objects, so dispose it
  @override
  void dispose() {
    _nipFocusNode.dispose();
    _jenisFocusNode.dispose();
    _nameFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Hero(
                  tag: 'check',
                  child: Image.asset(
                    'assets/images/ecorp.png',
                    filterQuality: FilterQuality.high,
                    height: 30,
                  ),
                ),
              ),
              HeroText(
                Constants.formCuti,
                tag: Constants.titleTag,
                viewState: ViewState.shrunk,
                style: LoginThemeHelper.loginTextStyle,
              ),
              const SizedBox(width: 20),
            ],
          ),
        ),
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formGlobalKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                // TextFormField(
                //   controller: _NIPController,
                //   autofocus: true,
                //   // focusNode: _nipFocusNode,
                //   textInputAction: TextInputAction.next,
                //   // onFieldSubmitted: (_) {
                //   //   FocusScope.of(context).requestFocus(_nipFocusNode);
                //   // },
                //   decoration: InputDecoration(
                //     hintText: "NIP",
                //     icon: Icon(Icons.person),
                //   ),
                //   keyboardType: TextInputType.text,
                //   validator: (inputText) {
                //     if (inputText!.isEmpty) return "Please Enter your name";
                //     return null;
                //   },
                // ),
                SizedBox(
                  height: 20,
                ),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.pin_end_rounded),
                  ),
                  hint: _selectedText == ""
                      ? Text("Jenis Cuti...")
                      : Text(_selectedText),
                  value: _valJenisCuti,
                  items: _dataListCuti
                      .map((eachItem) => DropdownMenuItem(
                    value: eachItem["id_cuti"],
                    child: Text(eachItem["nama_cuti"]),
                  ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      // Will unfocus other fields, otherwise after selecting
                      // a value last active focus node will be focued
                      FocusScope.of(context).requestFocus(new FocusNode());
                      _valJenisCuti = value as String?;
                      // _selectedText = values;
                    });

                    // print(_selectedText);
                  },
                  validator: (value) {
                    if (value == null) return "Select an option";
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
              TextField(
                controller: dateInput,
                //editing controller of this TextField
                decoration: InputDecoration(
                    icon: Icon(Icons.calendar_today), //icon of text field
                    labelText: "Tanggal Mulai" //label text of field
                ),
                readOnly: true,
                //set it true, so that user will not able to edit text
                onTap: () async {
                  DateTime? pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1950),
                      //DateTime.now() - not to allow to choose before today.
                      lastDate: DateTime(2100));

                  if (pickedDate != null) {
                    print(
                        pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                    String formattedDate =
                    DateFormat('yyyy-MM-dd').format(pickedDate);
                    print(
                        formattedDate); //formatted date output using intl package =>  2021-03-16
                    setState(() {
                      dateInput.text =
                          formattedDate; //set output date to TextField value.
                    });
                  } else {}
                },
              ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: lastDateInput,
                  //editing controller of this TextField
                  decoration: InputDecoration(
                      icon: Icon(Icons.calendar_today), //icon of text field
                      labelText: "Tanggal Terakhir" //label text of field
                  ),
                  readOnly: true,
                  //set it true, so that user will not able to edit text
                  onTap: () async {
                    DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1950),
                        //DateTime.now() - not to allow to choose before today.
                        lastDate: DateTime(2100));

                    if (pickedDate != null) {
                      print(
                          pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                      String formattedDate =
                      DateFormat('yyyy-MM-dd').format(pickedDate);
                      print(
                          formattedDate); //formatted date output using intl package =>  2021-03-16
                      setState(() {
                        lastDateInput.text =
                            formattedDate; //set output date to TextField value.
                      });
                    } else {}
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _JumlahCutiController,
                  textInputAction: TextInputAction.next,
                  // focusNode: _jenisFocusNode,
                  // onFieldSubmitted: (_) {
                  //   _jenisFocusNode.unfocus();
                  // },
                  decoration: InputDecoration(
                    hintText: "Jumlah Cuti",
                    icon: Icon(Icons.brush),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (inputText) {
                    if (inputText!.isEmpty)
                      return "Please Enter your email";
                    else if (!inputText.contains('@'))
                      return "Not a valid email address";
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _KeteranganController,
                  textInputAction: TextInputAction.next,
                  // focusNode: _jenisFocusNode,
                  // onFieldSubmitted: (_) {
                  //   _jenisFocusNode.unfocus();
                  // },
                  decoration: InputDecoration(
                    hintText: "Keterangan",
                    icon: Icon(Icons.edit_note),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (inputText) {
                    if (inputText!.isEmpty)
                      return "Please Enter your email";
                    else if (!inputText.contains('@'))
                      return "Not a valid email address";
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _AlamatController,
                  textInputAction: TextInputAction.next,
                  // focusNode: _jenisFocusNode,
                  // onFieldSubmitted: (_) {
                  //   _jenisFocusNode.unfocus();
                  // },
                  decoration: InputDecoration(
                    hintText: "Alamat Sementara",
                    icon: Icon(Icons.live_help),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (inputText) {
                    if (inputText!.isEmpty)
                      return "Please Enter your email";
                    else if (!inputText.contains('@'))
                      return "Not a valid email address";
                    return null;
                  },
                ),

                SizedBox(
                  height: 80,
                ),
                Container(
                  width: double.infinity,
                  child: ElevatedButton.icon(
                    icon: const Icon(
                      Icons.save,
                      color: Colors.white,
                      size: 24.0,
                    ),
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: const BorderSide(
                                color: Colors.blueAccent,
                                width: 1.0),
                          )),
                      backgroundColor: MaterialStateProperty.resolveWith((states) {
                        // If the button is pressed, return green, otherwise blue
                        if (states.contains(MaterialState.pressed)) {
                          return Colors.green;
                        }
                        return Colors.blue;
                      }),
                      textStyle: MaterialStateProperty.resolveWith((states) {
                        // If the button is pressed, return size 40, otherwise 20
                        if (states.contains(MaterialState.pressed)) {
                          return TextStyle(fontSize: 40);
                        }
                        return TextStyle(fontSize: 20);
                      }),
                    ),
                    onPressed: () async {
                      var res = await api.postPengjuanCuiti(
                          _valJenisCuti,
                          dateInput.text,
                          lastDateInput.text,
                          _JumlahCutiController.text,
                          _KeteranganController.text,
                          _AlamatController.text,
                      );
                      var payload = {
                      _valJenisCuti,
                      dateInput.text,
                      lastDateInput.text,
                      _JumlahCutiController.text,
                      _KeteranganController.text,
                      _AlamatController.text,
                      };
                      print(payload);
                      switch (res.statusCode){
                        case 200:
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => SecondScreen())); }

                      },
                    label: const Text('Simpan',
                        style: TextStyle(
                          height: 1.171875,
                          fontSize: 24.0,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
