import 'package:flutter/material.dart';
import 'package:flutter_login/theme.dart';
import 'package:flutter_login/widgets.dart';
import 'package:login_example/screens/second_screen.dart';

import '../helper/constants.dart';

class ReportCutiScreen extends StatefulWidget {
  @override
  _ReportCutiScreenState createState() => _ReportCutiScreenState();
}


class _ReportCutiScreenState extends State<ReportCutiScreen> {
  final _formGlobalKey = GlobalKey<FormState>();
  String _selectedText = "";
  List<String> _cutiType = [
    "Tahunan",
    "Sakit",
    "Nikahan",
    "Hamil",
    "Lahiran",
    "Pindah rumah",
  ];

  late FocusNode _phoneFocusNode, _emailFocusNode, _nameFocusNode;

  // Initialize Focus Node Objects in initState
  @override
  void initState() {
    super.initState();

    _phoneFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _nameFocusNode = FocusNode();
  }

  // Focus nodes are long living objects, so dispose it
  @override
  void dispose() {
    _phoneFocusNode.dispose();
    _emailFocusNode.dispose();
    _nameFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Hero(
                  tag: 'check',
                  child: Image.asset(
                    'assets/images/ecorp.png',
                    filterQuality: FilterQuality.high,
                    height: 30,
                  ),
                ),
              ),
              HeroText(
                Constants.reportCuti,
                tag: Constants.titleTag,
                viewState: ViewState.shrunk,
                style: LoginThemeHelper.loginTextStyle,
              ),
              const SizedBox(width: 20),
            ],
          ),
        ),
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formGlobalKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.search),
                  ),
                  hint: _selectedText == ""
                      ? Text("Jenis Cuti....")
                      : Text(_selectedText),
                  items: _cutiType
                      .map((eachItem) => DropdownMenuItem(
                    value: eachItem,
                    child: Text(eachItem),
                  ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      // Will unfocus other fields, otherwise after selecting
                      // a value last active focus node will be focued
                      FocusScope.of(context).requestFocus(new FocusNode());
                      // _selectedText = values;
                    });

                    // print(_selectedText);
                  },
                  validator: (value) {
                    if (value == null) return "Select an option";
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  textInputAction: TextInputAction.next,
                  focusNode: _emailFocusNode,
                  onFieldSubmitted: (_) {
                    _emailFocusNode.unfocus();
                  },
                  decoration: InputDecoration(
                    hintText: "Tanggal Mulai",
                    icon: Icon(Icons.calendar_month),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (inputText) {
                    if (inputText!.isEmpty)
                      return "Please Enter your email";
                    else if (!inputText.contains('@'))
                      return "Not a valid email address";
                    return null;
                  },
                ),

                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  textInputAction: TextInputAction.next,
                  focusNode: _emailFocusNode,
                  onFieldSubmitted: (_) {
                    _emailFocusNode.unfocus();
                  },
                  decoration: InputDecoration(
                    hintText: "Tanggal Selesai",
                    icon: Icon(Icons.calendar_month),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (inputText) {
                    if (inputText!.isEmpty)
                      return "Please Enter your email";
                    else if (!inputText.contains('@'))
                      return "Not a valid email address";
                    return null;
                  },
                ),
                SizedBox(
                  height: 80,
                ),
                Container(
                  width: double.infinity,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.blue,
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SecondScreen())); },
                    child: Text('Generate'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
