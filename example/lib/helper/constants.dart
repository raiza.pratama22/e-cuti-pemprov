class Constants {
  static const String appName = 'E-Cuti APPS';
  static const String logoTag = 'near.huscarl.loginsample.logo';
  static const String titleTag = 'near.huscarl.loginsample.title';
  static const String formCuti = 'Form Cuti Karyawan';
  static const String sisaCuti = 'Sisa Cuti Karyawan' ;
  static const String historyCuti = 'History Cuti Karyawan' ;
  static const String approveCuti = 'Approval Cuti Karyawan' ;
  static const String reportCuti = 'Report Cuti Karyawan' ;
  static const String BASE_URL = 'http://fatazaid.com/pengajuan_cuti/api-cuti-pemda';
}
